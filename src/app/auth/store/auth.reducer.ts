import { User } from '../user.model';
import * as AuthAction from './app.actions';

export interface State {
  user: User;
  loading: boolean;
  authError: string;
  authData: { email: string; passord: string };
}
const intialState: State = {
  user: null,
  loading: false,
  authError: null,
  authData: null
};
export function AuthReducer(state: State = intialState, action: AuthAction.AuthActions) {
  switch (action.type) {
    case AuthAction.LOGIN:
      return {
        ...state,
        user: new User(
          action.payload.email,
          action.payload.id,
          action.payload.token,
          action.payload.expirationDate
        ),
        loading: false,
        authError: null
      };
    case AuthAction.LOGOUT:
      return {
        ...state,
        user: null
      };
    case AuthAction.LOGIN_START:
      return {
        ...state,
        loading: true,
        authError: null
      };
    case AuthAction.LOGIN_FAIL:
      return {
        ...state,
        loading: false,
        authError: action.payload.message
      };
    case AuthAction.SIGNUP_START:
      return {
        ...state,
        authData: { email: action.payload.email, passord: action.payload.password }
      };
    default:
      return state;
  }
}
