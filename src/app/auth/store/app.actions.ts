import { Action } from '@ngrx/store';

export const LOGIN = '[Auth] LOGIN';
export const LOGOUT = '[Auth] LOGOUT';
export const LOGIN_START = '[Auth] LOGIN_START';
export const LOGIN_FAIL = '[Auth] LOGIN_FAIL';

export const SIGNUP = '[Auth] SignUp';
export const SIGNUP_START = '[Auth] SignUp Start';
export const SIGNUP_FAIL = '[Auth] SignUp Fail';

export class Login implements Action {
  readonly type = LOGIN;
  constructor(public payload: { email: string; id: string; token: string; expirationDate: Date }) {}
}
export class Logout implements Action {
  readonly type = LOGOUT;
}
export class LoginStart implements Action {
  readonly type = LOGIN_START;
  constructor(public payload: { email: string; password: string }) {}
}
export class LoginFail implements Action {
  readonly type = LOGIN_FAIL;
  constructor(public payload: { message: string }) {}
}

export class SignUpStart implements Action {
  readonly type = SIGNUP_START;
  constructor(public payload: { email: string; password: string }) {}
}

export type AuthActions = Login | Logout | LoginStart | LoginFail | SignUpStart;
