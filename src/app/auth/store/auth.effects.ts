import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap, map, catchError, tap } from 'rxjs/operators';

import * as AuthActions from './app.actions';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

interface AuthenticationResponse {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: string;
}

@Injectable()
export class AuthEffects {
  @Effect()
  authLogin = this.actions$.pipe(
    ofType(AuthActions.LOGIN_START),
    switchMap((authData: AuthActions.LoginStart) => {
      console.log(authData);
      return this.postAuthenticationData(authData).pipe(
        map(this.handleAuthenticationResponse),
        catchError(this.handleAuthenticationError)
      );
    })
  );

  @Effect({ dispatch: false })
  authLoginSuccess = this.actions$.pipe(
    ofType(AuthActions.LOGIN),
    tap(authState => {
      this.router.navigate(['/']);
    })
  );
  constructor(private actions$: Actions, private http: HttpClient, private router: Router) {}

  private postAuthenticationData(authData: AuthActions.LoginStart) {
    return this.http.post<AuthenticationResponse>(
      'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDduOYBK-MZbcOE7BKcSILIOpoFeJI8yAo',
      {
        email: authData.payload.email,
        password: authData.payload.password,
        returnSecureToken: true
      }
    );
  }

  private handleAuthenticationResponse(authResponse) {
    console.log(authResponse);
    const expirationDate = new Date(new Date().getTime() + +authResponse.expiresIn * 1000);
    return new AuthActions.Login({
      email: authResponse.email,
      id: authResponse.localId,
      token: authResponse.idToken,
      expirationDate: expirationDate
    });
  }

  private handleAuthenticationError(errorResponse: HttpErrorResponse) {
    let errorMessage = 'An unknown error occurred';
    try {
      errorMessage = errorResponse.error.error.message;
    } catch (error) {}
    return of(new AuthActions.LoginFail({ message: errorMessage }));
  }
}
