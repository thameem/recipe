import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from './auth.service';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as AuthActions from './store/app.actions';
import * as fromAppState from '../store/app.reducer';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {
  isLoginMode = true;
  isLoading = false;
  error: string = null;
  // errorSubscription: Subscription;
  userLoginSubscription: Subscription;
  constructor(private authService: AuthService, private store: Store<fromAppState.AppState>) {}
  ngOnInit() {
    // this.errorSubscription = this.authService.error.subscribe(errorMessage => {
    //   this.error = errorMessage;
    // });
    this.userLoginSubscription = this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.loading;

      if (authState.authError != null) {
        this.error = authState.authError;
      }
    });
  }
  onSignUp(authForm: NgForm) {
    let observable = new Observable<any>();
    this.isLoading = true;
    if (this.isLoginMode) {
      this.store.dispatch(
        new AuthActions.LoginStart({
          email: authForm.value.email,
          password: authForm.value.password
        })
      );
    } else {
      observable = this.authService.SignUp(authForm.value.email, authForm.value.password);
    }
    this.userLoginSubscription = observable.subscribe(
      responser => {
        this.isLoading = false;
      },
      error => {
        this.isLoading = false;
      }
    );
    authForm.reset();
  }
  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  ngOnDestroy() {
    // this.errorSubscription.unsubscribe();
    this.userLoginSubscription.unsubscribe();
  }
}
