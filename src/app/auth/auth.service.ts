import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Subject, throwError, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { User } from './user.model';
import * as fromAppState from '../store/app.reducer';
import * as AuthActions from './store/app.actions';

interface AuthenticationResponse {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: string;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  error = new Subject<string>();
  // user = new BehaviorSubject<User>(null);
  private API_KEY = 'AIzaSyDduOYBK-MZbcOE7BKcSILIOpoFeJI8yAo';
  constructor(
    private http: HttpClient,
    private router: Router,
    private store: Store<fromAppState.AppState>
  ) {}

  Login(email: string, password: string) {
    return this.http
      .post<AuthenticationResponse>(
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword',
        { email, password, returnSecureToken: true },
        {
          params: new HttpParams().set('key', this.API_KEY)
        }
      )
      .pipe(
        tap(response => {
          const user = new User(
            email,
            response.localId,
            response.idToken,
            new Date(new Date().getTime() + +response.expiresIn * 1000)
          );
          const expirationDate: Date = new Date(new Date().getTime() + +response.expiresIn * 1000);
          // console.log(
          //   'Loggen In User: ',
          //   new Date(new Date().getMilliseconds() + +response.expiresIn * 1000),
          //   user.token
          // );
          localStorage.setItem('userData', JSON.stringify(user));
          // this.user.next(user);
          this.store.dispatch(
            new AuthActions.Login({
              email: email,
              id: response.localId,
              token: response.idToken,
              expirationDate: expirationDate
            })
          );
          this.router.navigate(['/']);
        }),
        catchError((errorResponse: HttpErrorResponse) => {
          console.log(errorResponse);
          this.error.next(errorResponse.error.error.message);
          return throwError(errorResponse.message);
        })
      );
  }

  Logout() {
    // this.user.next(null);
    this.store.dispatch(new AuthActions.Logout());
    localStorage.removeItem('userData');
    this.router.navigate(['auth']);
  }

  AutoLogin() {
    const userData = JSON.parse(localStorage.getItem('userData'));
    if (userData != null) {
      const loadedUser = new User(
        userData.email,
        userData.id,
        userData._token,
        new Date(userData._expirationDate)
      );
      if (loadedUser.token != null) {
        // this.user.next(loadedUser);
        this.store.dispatch(
          new AuthActions.Login({
            email: userData.email,
            id: userData.id,
            token: userData._token,
            expirationDate: new Date(userData._expirationDate)
          })
        );
      } else {
        localStorage.removeItem('userData');
      }
    }
  }

  SignUp(email: string, password: string) {
    return this.http
      .post<AuthenticationResponse>(
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser',
        { email, password, returnSecureToken: true },
        {
          params: new HttpParams().set('key', this.API_KEY)
        }
      )
      .pipe(
        catchError((errorResponse: HttpErrorResponse) => {
          this.error.next(errorResponse.error.error.message);
          return throwError(errorResponse.message);
        })
      );
  }
}
