import { HttpInterceptor, HttpRequest, HttpHandler, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { take, exhaustMap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromAppState from '../store/app.reducer';
import * as AuthActions from './store/app.actions';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private store: Store<fromAppState.AppState>) {}
  intercept(req: HttpRequest<any>, handler: HttpHandler) {
    return this.store.select('auth').pipe(
      take(1),
      map(authState => authState.user),
      exhaustMap(user => {
        let params = req.params;
        console.log(params);
        if (user != null) {
          params = params.append('auth', user.token);
        }
        const updateReq = req.clone({
          params: params
        });
        return handler.handle(updateReq);
      })
    );
  }
}
