export class User {
  constructor(
    public email: string,
    public id: string,
    public _token: string,
    public _tokenExpirationDate: Date
  ) {}

  public get token(): string {
    if (this._tokenExpirationDate == null || this._tokenExpirationDate < new Date()) {
      this._token = null;
      return null;
    }
    return this._token;
  }
}
