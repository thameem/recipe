import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import * as fromAppState from '../store/app.reducer';
import * as AuthActions from '../auth/store/app.actions';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private store: Store<fromAppState.AppState>) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    console.log('Auth Guard');
    return this.store.select('auth').pipe(
      map(authState => authState.user),
      map(user => {
        console.log('Auth Guard', user);
        if (user != null) {
          return true;
        }
      })
    );
  }
}
