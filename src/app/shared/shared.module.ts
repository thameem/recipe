import { NgModule } from '@angular/core';

import { DropdownDirective } from './dropdown.directive';
import { AppLoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [DropdownDirective, AppLoaderComponent],
  exports: [DropdownDirective, AppLoaderComponent]
})
export class SharedModule {}
