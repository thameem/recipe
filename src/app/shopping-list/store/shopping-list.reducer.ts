import { Ingredient } from '../../shared/ingredient.model';
import * as ShoppingListAction from './shopping-list.actions';

export interface State {
  ingredients: Ingredient[];
  editIngredient: Ingredient;
  editIngredientIndex: number;
}

const intialState: State = {
  ingredients: [new Ingredient('Apples', 5), new Ingredient('Tomatoes', 10)],
  editIngredient: null,
  editIngredientIndex: -1
};
export function shoppingListReducer(
  state: State = intialState,
  action: ShoppingListAction.ShoppingListActions
) {
  switch (action.type) {
    case ShoppingListAction.ADD_INGREDIENT:
      return {
        ...state,
        ingredients: [...state.ingredients, action.payload]
      };
    case ShoppingListAction.ADD_INGREDIENTS:
      return {
        ...state,
        ingredients: [...state.ingredients, ...action.payload]
      };
    case ShoppingListAction.START_EDIT:
      return {
        ...state,
        editIngredient: { ...state.ingredients[action.payload] },
        editIngredientIndex: action.payload
      };
    case ShoppingListAction.STOP_EDIT:
      return {
        ...state,
        editIngredient: null,
        editIngredientIndex: -1
      };
    case ShoppingListAction.UPDATE_INGREDIENT:
      const stateCopy = [...state.ingredients];
      stateCopy[state.editIngredientIndex] = { ...action.payload };
      return {
        ...state,
        ingredients: [...stateCopy],
        editIngredient: null,
        editIngredientIndex: -1
      };
    case ShoppingListAction.DELETE_INGREDIENT:
      return {
        ...state,
        ingredients: [
          ...state.ingredients.filter((ingredient, index) => index !== state.editIngredientIndex)
        ],
        editIngredient: null,
        editIngredientIndex: -1
      };
    default:
      return state;
  }
}
