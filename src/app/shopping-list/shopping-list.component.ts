import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromShoppingList from './store/shopping-list.reducer';
import * as fromAPP from '../store/app.reducer';
import * as ShoppingListActions from './store/shopping-list.actions';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  ingredients: Observable<fromShoppingList.State>;
  // private subscription: Subscription;

  constructor(private store: Store<fromAPP.AppState>) {}

  ngOnInit() {
    this.ingredients = this.store.select('shoppingList');
  }
  onEditItem(i: number) {
    this.store.dispatch(new ShoppingListActions.EditStart(i));
  }
  ngOnDestroy() {}
}
