import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import { Ingredient } from '../../shared/ingredient.model';
import * as ShoppingListAction from '../store/shopping-list.actions';
import * as fromApp from '../../store/app.reducer';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @ViewChild('f', { static: true }) f: NgForm;
  onEditSubscribtion: Subscription;
  editMode = false;
  editedItem: Ingredient;
  editedItemIndex: number;
  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.onEditSubscribtion = this.store.select('shoppingList').subscribe(state => {
      if (state.editIngredientIndex === -1) {
        this.editMode = false;
        return;
      }
      this.editMode = true;
      this.editedItemIndex = state.editIngredientIndex;
      this.editedItem = state.editIngredient;
      this.f.setValue({ name: this.editedItem.name, amount: this.editedItem.amount });
    });
  }

  onAddItem(f: NgForm) {
    const newIngredient = new Ingredient(f.value.name, f.value.amount);
    if (this.editMode) {
      this.store.dispatch(new ShoppingListAction.UpdateIngredient(newIngredient));
    } else {
      this.store.dispatch(new ShoppingListAction.AddIngredient(newIngredient));
    }
    this.clearForm();
  }
  onDeleteItem() {
    this.store.dispatch(new ShoppingListAction.DeleteIngredient());
    this.onResetItem();
  }
  onResetItem() {
    this.clearForm();
  }
  clearForm() {
    this.f.reset();
    this.editMode = false;
    this.store.dispatch(new ShoppingListAction.EditStop());
  }
  ngOnDestroy() {
    //this.store.dispatch(new ShoppingListAction.EditStop());
    this.onEditSubscribtion.unsubscribe();
  }
}
