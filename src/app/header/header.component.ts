import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromAppState from '../store/app.reducer';
import * as AuthActions from '../auth/store/app.actions';
import { DataStorageService } from '../data-storage.service';
import { AuthService } from '../auth/auth.service';
import { RecipeService } from '../recipes/recipe.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
  isLoggedIn = false;
  userSubscription: Subscription;
  constructor(
    private dataStorageService: DataStorageService,
    private authService: AuthService,
    private recipeService: RecipeService,
    private store: Store<fromAppState.AppState>
  ) {}

  ngOnInit() {
    this.userSubscription = this.store
      .select('auth')
      .pipe(map(authState => authState.user))
      .subscribe(user => {
        this.isLoggedIn = !!user;
        console.log(this.isLoggedIn);
      });
  }

  onSaveRecipes() {
    this.dataStorageService.saveRecipes();
  }

  onFetchRecipes() {
    this.dataStorageService.fetchRecipes().subscribe();
  }

  onLogout() {
    this.authService.Logout();
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
