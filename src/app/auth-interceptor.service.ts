import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newreq = req.clone({ headers: req.headers.append('Auth', 'XYZ') });
    console.log('Request is on its way!!!');
    newreq.headers.append('Auth', 'test');
    return next.handle(newreq).pipe(
      tap(event => {
        console.log(event);
      })
    );
  }
}
