import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  id: number;
  editMode = false;
  recipeForm: FormGroup;
  editRecipe: Recipe;
  editRecipeIndex: number;
  paramSubscription: Subscription;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private recipeService: RecipeService
  ) {}

  ngOnInit() {
    this.paramSubscription = this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.editMode = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    let recipeName = '';
    let recipeDescription = '';
    let recipeImagePath = '';
    const recipeIngredients: FormArray = new FormArray([]);
    if (this.editMode) {
      this.editRecipe = this.recipeService.getRecipe(this.id);
      recipeName = this.editRecipe.name;
      recipeDescription = this.editRecipe.description;
      recipeImagePath = this.editRecipe.imagePath;
      if (this.editRecipe['ingredients']) {
        this.editRecipe['ingredients'].forEach(ingredient => {
          recipeIngredients.push(
            new FormGroup({
              name: new FormControl(ingredient.name),
              amount: new FormControl(ingredient.amount)
            })
          );
        });
      }
      this.recipeForm = new FormGroup({
        name: new FormControl(recipeName),
        description: new FormControl(recipeDescription),
        imagePath: new FormControl(recipeImagePath),
        ingredients: recipeIngredients
      });
    }

    this.recipeForm = new FormGroup({
      name: new FormControl(recipeName),
      imagePath: new FormControl(recipeImagePath),
      description: new FormControl(recipeDescription),
      ingredients: recipeIngredients
    });
  }
  getControls() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }
  onSave() {
    const recipeIngredient: Ingredient[] = [];
    this.recipeForm.value.ingredients.forEach(ingredient => {
      recipeIngredient.push(new Ingredient(ingredient.name, ingredient.amount));
    });
    const updatedRecipe: Recipe = new Recipe(
      this.recipeForm.value.name,
      this.recipeForm.value.description,
      this.recipeForm.value.imagePath,
      recipeIngredient
    );
    if (this.editMode) {
      this.recipeService.updateRecipe(this.id, updatedRecipe);
      this.router.navigate(['recipes', this.id]);
    } else {
      this.id = this.recipeService.addRecipe(this.recipeForm.value);
    }
    this.onCancel();
  }
  onCancel() {
    if (!this.editMode) {
      this.router.navigate(['/recipes']);
    } else {
      this.router.navigate(['/recipes', this.id]);
    }
  }
  onRemoveIngredient(index: number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }
  onAddNewIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        name: new FormControl(null),
        amount: new FormControl(null)
      })
    );
  }

  ngOnDestroy() {
    this.paramSubscription.unsubscribe();
  }
}
